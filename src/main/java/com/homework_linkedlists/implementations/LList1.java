package com.homework_linkedlists.implementations;

import com.homework_linkedlists.interfaces.ILinkedList;
import com.homework_linkedlists.nodes.Node1;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

public class LList1<T> implements ILinkedList<T> {
    private int size = 0;
    private Node1<T> first;
    private Node1<T> last;

    public LList1(){

    }

    public LList1(T[] array){
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    public LList1(Collection<T> collection){
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            T element = iterator.next();
            add(element);
            size++;
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (index == size - 1) {
            return last.item;
        }
        else if (index == 0) {
            return first.item;
        }
        else {
            T itemToGet = null;
            Node1<T> node1ToGet = first;
            for (int i = 1; i < size - 1; i++) {
                node1ToGet = node1ToGet.next;
                if (i == index) {
                    itemToGet = node1ToGet.item;
                }
            }
            return itemToGet;
        }
    }

    @Override
    public boolean add(T value) {
        if (value == null) {
            return false;
        }
        Node1<T> node1ToAdd = new Node1<>(value, null);
        if (last == null) {
            first = node1ToAdd;
        }
        else {
            last.next = node1ToAdd;
        }
        last = node1ToAdd;
        size++;
        return true;
    }

    @Override
    public boolean addFirst(T value) {
        if (value == null) {
            return false;
        }
        Node1<T> node1ToAdd = new Node1<>(value, first);
        if (last == null) {
            last = node1ToAdd;
        }
        first = node1ToAdd;
        size++;
        return true;
    }

    @Override
    public boolean addLast(T value) {
        return add(value);
    }

    @Override
    public boolean add(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index > size) {
            return false;
        }
        else {
            if (index == size) {
                return addLast(value);
            }
            else if (index == 0) {
                return addFirst(value);
            }
            else {
                Node1<T> prevNode1 = first;
                Node1<T> nextNode1 = null;
                for (int i = 1; i < size; i++) {
                    nextNode1 = prevNode1.next;
                    if (i == index) {
                        Node1<T> newNode1 = new Node1<>(value, nextNode1);
                        prevNode1.next = newNode1;
                        break;
                    }
                    prevNode1 = nextNode1;
                }
                size++;
                return true;
            }
        }
    }

    @Override
    public T remove(T element) {
        if (element == null) {
            return null;
        }
        else if (first.item.equals(element)) {
            T valToReturn = first.item;
            first = first.next;
            if (size == 1) {
                last = null;
            }
            size--;
            return valToReturn;
        }
        else {
            T valToReturn = null;
            Node1<T> prevNode1 = first;
            Node1<T> nextNode1 = null;
            for (int i = 1; i < size; i++) {
                nextNode1 = prevNode1.next;
                if (nextNode1.item.equals(element)) {
                    valToReturn = nextNode1.item;
                    prevNode1.next = nextNode1.next;
                    if (i == size - 1) {
                        last = prevNode1;
                    }
                    break;
                }
                prevNode1 = nextNode1;
            }
            if (valToReturn != null) {
                size--;
            }
            return valToReturn;
        }
    }

    @Override
    public T removeFirst() {
        return removeByIndex(0);
    }

    @Override
    public T removeLast() {
        return removeByIndex(size - 1);
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        else if (size == 1) {
            T valToReturn = last.item;
            first = null;
            last = null;
            size--;
            return valToReturn;
        }
        else if (index == 0) {
            T valToReturn = first.item;
            first = first.next;
            size--;
            return valToReturn;
        }
        else {
            T valToReturn = null;
            Node1<T> prevNode1 = first;
            Node1<T> nextNode1 = null;
            for (int i = 1; i < size; i++) {
                nextNode1 = prevNode1.next;
                if (i == index) {
                    valToReturn = nextNode1.item;
                    prevNode1.next = nextNode1.next;
                    if (i == size - 1) {
                        last = prevNode1;
                    }
                    break;
                }
                prevNode1 = nextNode1;
            }
            size--;
            return valToReturn;
        }
    }

    @Override
    public boolean contains(T value) {
        if (value == null) {
            return false;
        }
        else if (last.item.equals(value)) {
            return true;
        }
        else if (first.item.equals(value)) {
            return true;
        }
        else{
            boolean doesContain = false;
            Node1<T> currentNode1 = first;
            for (int i = 1; i < size - 1; i++) {
                currentNode1 = currentNode1.next;
                if (currentNode1.item.equals(value)) {
                    doesContain = true;
                    break;
                }
            }
            return doesContain;
        }
    }

    @Override
    public boolean set(int index, T value) {
        if (value == null) {
            return false;
        }
        else if (index < 0 || index >= size) {
            return false;
        }
        else if (index == size - 1) {
            last.item = value;
            return true;
        }
        else if (index == 0) {
            first.item = value;
            return true;
        }
        else {
            Node1<T> currentNode1 = first;
            for (int i = 1; i < size; i++) {
                currentNode1 = currentNode1.next;
                if (i == index) {
                    currentNode1.item = value;
                    break;
                }
            }
            return true;
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        if (size != 0) {
            T valToAdd = null;
            Node1<T> prevNode1 = first;
            valToAdd = first.item;
            stringArray += valToAdd;
            Node1<T> nextNode1 = null;
            for (int i = 1; i < size; i++) {
                stringArray += ", ";
                nextNode1 = prevNode1.next;
                stringArray += nextNode1.item;
                prevNode1 = nextNode1;
            }
        }
        stringArray += "]\n";
        System.out.println(stringArray);
    }

    @Override
    public Object[] toArray() {
        Object[] listArray = new Object[size];
        if (size != 0) {
            Node1<T> currentNode1 = first;
            listArray[0] = currentNode1.item;
            for (int i = 1; i < size; i++) {
                currentNode1 = currentNode1.next;
                listArray[i] = currentNode1.item;
            }
        }
        return listArray;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        else {
            for (int i = 0; i < arr.length; i++) {
                remove(arr[i]);
            }
            return true;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LList1<?> lList1 = (LList1<?>) o;
        return size == lList1.size && Objects.equals(first, lList1.first) && Objects.equals(last, lList1.last);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, first, last);
    }
}
