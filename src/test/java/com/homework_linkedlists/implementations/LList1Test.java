package com.homework_linkedlists.implementations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class LList1Test {
    LList1<Integer> cut = new LList1<>();

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
          Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54}), new LList1<Integer> ()),
          Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25}), new LList1<Integer> ())
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(LList1<Integer> testList, LList1<Integer> expected) {
        cut = testList;
        cut.clear();
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] getTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 0, 34),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 2, 19)
        };
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(LList1<Integer> testList, int index, int expected) {
        cut = testList;
        int actual = cut.get(index);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 22,
                        new LList1<Integer> (new Integer[] {34, 54, 36, 22})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 33,
                        new LList1<Integer> (new Integer[] {12, 25, 19, 33}))
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(LList1<Integer> testList, int number, LList1<Integer> expected) {
        cut = testList;
        cut.add(number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] addFirstTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 12,
                        new LList1<Integer> (new Integer[] {12, 34, 54, 36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 21,
                        new LList1<Integer> (new Integer[] {21, 12, 25, 19}))
        };
    }

    @ParameterizedTest
    @MethodSource("addFirstTestArgs")
    void addFirstTest(LList1<Integer> testList, int number, LList1<Integer> expected) {
        cut = testList;
        cut.addFirst(number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] addLastTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 55,
                        new LList1<Integer> (new Integer[] {34, 54, 36, 55})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 66,
                        new LList1<Integer> (new Integer[] {12, 25, 19, 66}))
        };
    }

    @ParameterizedTest
    @MethodSource("addLastTestArgs")
    void addLastTest(LList1<Integer> testList, int number, LList1<Integer> expected) {
        cut = testList;
        cut.addLast(number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] addIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 3, 14,
                        new LList1<Integer> (new Integer[] {34, 54, 36, 14})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 1, 87,
                        new LList1<Integer> (new Integer[] {12, 87, 25, 19}))
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestArgs")
    void addIndexTest(LList1<Integer> testList, int index, int number, LList1<Integer> expected) {
        cut = testList;
        cut.add(index, number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 54,
                        new LList1<Integer> (new Integer[] {34, 36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {12, 25, 19}), 19,
                        new LList1<Integer> (new Integer[] {12, 25}))
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(LList1<Integer> testList, int number, LList1<Integer> expected) {
        cut = testList;
        cut.remove(number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeFirstTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}),
                        new LList1<Integer> (new Integer[] {54, 36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19}),
                        new LList1<Integer> (new Integer[] {25, 19}))
        };
    }

    @ParameterizedTest
    @MethodSource("removeFirstTestArgs")
    void removeFirstTest(LList1<Integer> testList, LList1<Integer> expected) {
        cut = testList;
        cut.removeFirst();
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeLastTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}),
                        new LList1<Integer> (new Integer[] {34, 54})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19}),
                        new LList1<Integer> (new Integer[] {91, 25}))
        };
    }

    @ParameterizedTest
    @MethodSource("removeLastTestArgs")
    void removeLastTest(LList1<Integer> testList, LList1<Integer> expected) {
        cut = testList;
        cut.removeLast();
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 0,
                        new LList1<Integer> (new Integer[] {54, 36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19}), 2,
                        new LList1<Integer> (new Integer[] {91, 25}))
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(LList1<Integer> testList, int index, LList1<Integer> expected) {
        cut = testList;
        cut.removeByIndex(index);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] containsTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36, 65, 77}), 54, true),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19, 89, 88}), 11, false)
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(LList1<Integer> testList, int number, boolean expected) {
        cut = testList;
        boolean actual = cut.contains(number);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] setTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), 0, 77,
                        new LList1<Integer> (new Integer[] {77, 54, 36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19}), 1, 88,
                        new LList1<Integer> (new Integer[] {91, 88, 19}))
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(LList1<Integer> testList, int index, int number, LList1<Integer> expected) {
        cut = testList;
        cut.set(index, number);
        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36, 65, 77}),
                        (Object[]) new Integer[] {34, 54, 36, 65, 77}),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19, 89, 88}),
                        (Object[]) new Integer[] {91, 25, 19, 89, 88})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(LList1<Integer> testList, Object[] expected) {
        cut = testList;
        Object[] actual = cut.toArray();
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] removeAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new LList1<Integer> (new Integer[] {34, 54, 36}), new Integer[] {21, 34, 4 ,54},
                        new LList1<Integer> (new Integer[] {36})),
                Arguments.arguments(new LList1<Integer> (new Integer[] {91, 25, 19}), new Integer[] {91, 19, 81, 9},
                        new LList1<Integer> (new Integer[] {25}))
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(LList1<Integer> testList, Integer[] numbers, LList1<Integer> expected) {
        cut = testList;
        cut.removeAll(numbers);
        Assertions.assertEquals(expected, cut);
    }
}
